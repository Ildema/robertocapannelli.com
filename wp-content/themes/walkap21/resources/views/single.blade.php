@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  @while(have_posts()) @php the_post() @endphp
  @include('partials.content-single-'.get_post_type())
  @endwhile
@endsection
