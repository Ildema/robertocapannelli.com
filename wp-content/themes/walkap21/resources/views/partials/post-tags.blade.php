@if(!empty($terms = get_the_terms(null, 'post_tag')))
  <div class="posts-tags d-inline-block mb-2">
    in
    @foreach($terms as $term)
      <a href="{{get_term_link($term->term_id)}}" class="post-tag font-weight-bold">
        {{$term->name}}
      </a>
    @endforeach
  </div>
@endif
