<article @php post_class() @endphp itemscope="" itemtype="https://schema.org/Book">
  <div class="row">
    <div class="col-md-3">
      @if(has_post_thumbnail())
        <figure>
          <a href="{{\App\Controllers\SingleBook::getAmazonReferral()}}" target="_blank">
            {!! get_the_post_thumbnail(null, 'post-thumbnail', ['class' => 'img-fluid w-100 h-auto', 'itemprop' => 'image']) !!}
          </a>
        </figure>
      @endif
    </div>

    <div class="col-md-9">
      <header class="mb-3">
        @include('partials/post-tags')
        <h1 class="entry-title" itemprop="name">
          {!! get_the_title() !!}
        </h1>
        <span class="text-muted d-block" itemprop="author" itemtype="https://schema.org/Person">
          {{\App\Controllers\SingleBook::getAuthor()}}
        </span>
        <span class="text-muted">
          {{\App\Controllers\SingleBook::getPublicationYear()}}
        </span>
      </header>
      <div class="entry-content">
        @php the_content() @endphp
      </div>
      <footer>
        Se ti interessa questo libro acquistalo su
        <a href="{{\App\Controllers\SingleBook::getAmazonReferral()}}" class="text-referral" target="_blank">Amazon</a>
      </footer>
    </div>
  </div>
  {{--@include('partials.posts.related-posts')--}}
  @php comments_template('/partials/comments.blade.php') @endphp
</article>


