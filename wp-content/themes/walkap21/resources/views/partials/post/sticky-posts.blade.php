@if(isset(get_option('sticky_posts')[0]))
  <section class="mb-4">
    <h2>In evidenza</h2>
    @while($get_sticky_posts->have_posts())
      @php $get_sticky_posts->the_post(); @endphp
      @include('partials.content-'.get_post_type())
    @endwhile

    @php wp_reset_postdata() @endphp
  </section>
@endif
