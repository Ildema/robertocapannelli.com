@if($get_referral_links)
  <section class="referral-links py-4">
    <ul class="list-unstyled">
      @foreach($get_referral_links as $element)
        <li class="mb-4">
          <h4>{{$element['title']}}</h4>
          <ul class="">
            @foreach($element['referral_elements'] as $referral)
              @php //var_dump($referral) @endphp
              <li class="mb-2">
                {{$referral['label']}}: <a href="{{$referral['url']}}" target="_blank" class="text-referral">{{$referral['name']}}</a>
              </li>
            @endforeach
          </ul>
        </li>
        <hr>
      @endforeach
    </ul>
  </section>
@endif
