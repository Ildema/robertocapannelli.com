import Swiper, {Autoplay} from 'swiper';

Swiper.use([Autoplay]);

export default {
  init() {
    // JavaScript to be fired on the home page

    /* eslint-disable */
    var article_slider = new Swiper('.articles-slider', {
      slidesPerView: 1,
      spaceBetween: 15,
      loop: true,
      parallax: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      breakpoints: {
        1200: {
          slidesPerView: 2
        },
        991: {
          slidesPerView: 2
        },
        768: {
          slidesPerView: 2
        }
      },
    });

    var books_slider = new Swiper('.books-slider', {
      slidesPerView: 1,
      spaceBetween: 15,
      loop: true,
      parallax: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      breakpoints: {
        1200: {
          slidesPerView: 3
        },
        991: {
          slidesPerView: 3
        },
        768: {
          slidesPerView: 3
        }
      },
    });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
