import Mmenu from 'mmenu-js';

export default {
  init() {
    // JavaScript to be fired on all pages
    new Mmenu('#my-menu', {
        // options
        setSelected: {
          'hover': true,
        },
        extensions: [
          'pagedim-black',
          'fx-listitems-slide',
          'border-full',
        ],
        wrappers: ['bootstrap4'],
        navbar: {
          title: 'Roberto Capannelli',
        },
      },
      {
        // configuration
        offCanvas: {
          page: {
            selector: '#my-page',
          },
        },
        classNames: {
          fixedElements: {
            fixed: 'fix',
          },
        },
      });

    $('a[href*="#"]:not([href="#"])').click(function () {
      if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
        let target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - 80,
          }, 1200);
          return false;
        }
      }
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
