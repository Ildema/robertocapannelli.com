<?php


namespace App\Controllers\Partials;


trait Social {
    public function getSocialLinks(){
        return get_field('links', 'option');
    }
}
