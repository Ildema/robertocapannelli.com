<?php


namespace App\Controllers\Partials;


trait Posts {

    public function getLatestPost() {
        $transient = 'latest_posts';

        if ( WP_DEBUG || false === ( $posts = get_transient( $transient ) ) ) {
            $args = [
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => 2,
                'order'          => 'DESC',
                'orderby'        => 'date',
                'hide_empty'     => true,
                'post__not_in'   => get_option( 'sticky_posts' )
            ];

            $posts = new \WP_Query( $args );
            set_transient( $transient, $posts, MONTH_IN_SECONDS );
        }

        return $posts;
    }

    public function getWordpressPosts() {
        $transient = 'posts_wordpress_tag';

        if ( WP_DEBUG || false === ( $posts = get_transient( $transient ) ) ) {
            $args = [
                'post_type'      => 'post',
                'post_status'    => 'publish',
                'posts_per_page' => 4,
                'hide_empty'     => true,
                'order'          => 'DESC',
                'orderby'        => 'date',
                'taxonomy'       => 'post_tag',
                'tag'            => 'wordpress'
            ];

            $posts = new \WP_Query( $args );
            set_transient( $transient, $posts, MONTH_IN_SECONDS );
        }

        return $posts;
    }

    public function getStickyPosts() {
        $transient = 'sticky_posts';

        if ( WP_DEBUG || false === ( $posts = get_transient( $transient ) ) ) {

            $args = [
                'post_type'           => 'post',
                'post_status'         => 'publish',
                'posts_per_page'      => 1,
                'hide_empty'          => true,
                'order'               => 'DESC',
                'orderby'             => 'date',
                'post__in'            => get_option( 'sticky_posts' ),
                'ignore_sticky_posts' => 1
            ];

            $posts = new \WP_Query( $args );
            set_transient( $transient, $posts, MONTH_IN_SECONDS );
        }

        return $posts;
    }
}
