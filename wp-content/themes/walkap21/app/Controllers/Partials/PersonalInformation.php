<?php

namespace App\Controllers\Partials;

trait PersonalInformation {

    public function getVatNumber() {
        return get_field( 'vat_number', 'option' );
    }

    public function getName(){
        return get_field('name', 'option');
    }
}
