<?php


namespace App\Controllers\Partials;


trait Book {
    public static function getAuthor() {
        return get_field( 'author' );
    }

    public static function getPublicationYear() {
        return get_field( 'published' );
    }

    public static function getAmazonReferral() {
        return get_field( 'amazon_referral' );
    }

    public function getArchiveDescription() {
        return get_field( 'archive_description', 'option' );
    }

    public function getBookTerms() {
        $args = [ 'taxonomy'   => 'book-genre',
                  'hide_empty' => true,
                  'order_by'   => 'name',
                  'parent'     => 0 ];

        $terms = get_terms( $args );

        if ( empty( $terms ) || is_wp_error( $terms ) ) {
            return;
        }

        return $terms;
    }
}
