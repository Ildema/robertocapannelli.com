<?php


namespace App\Controllers;

use App\Controllers\Partials\Book;
use Sober\Controller\Controller;

class ArchiveBook extends Controller {
    use Book;
}
