<?php


namespace App\Controllers;


use App\Controllers\Partials\Book;
use Sober\Controller\Controller;

class SingleBook extends Controller {
    use Book;
}
