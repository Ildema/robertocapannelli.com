<?php


namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;


class SinglePost extends Controller {

    public function getRelatedPosts() {
        global $post;
        $tags    = wp_get_post_tags( $post->ID );
        $tag_ids = [];
        $args    = [];

        foreach ( $tags as $individual_tag ) {
            $tag_ids[] = $individual_tag->term_id;
            $args      = [
                'tag__in'        => $tag_ids,
                'post__not_in'   => array( $post->ID ),
                'posts_per_page' => 4,
            ];
        }

        return new WP_Query( $args );

    }

    public function getReferralLinks(){
        return get_field('referrals');
    }


}
