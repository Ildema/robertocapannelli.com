<?php

namespace App;

use App\Controllers\App;

/**
 * Theme customizer
 */
add_action( 'customize_register', function ( \WP_Customize_Manager $wp_customize ) {
    // Add postMessage support
    $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial( 'blogname', [
        'selector'        => '.brand',
        'render_callback' => function () {
            bloginfo( 'name' );
        }
    ] );
} );

/**
 * Customizer JS
 */
add_action( 'customize_preview_init', function () {
    wp_enqueue_script( 'sage/customizer.js', asset_path( 'scripts/customizer.js' ), [ 'customize-preview' ], null, true );
} );

add_action( 'wp_head', function () {
    ?>
    <script data-ad-client="ca-pub-5271751050627846" async
            src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <?php
} );

add_action( 'wp_head', function () {
    if ( is_user_logged_in() == 0 && wp_get_environment_type() !== 'local' ) :
        $ga_id = get_field( 'google_analytics', 'option' );

        if ( $ga_id ):
            ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=<?= $ga_id ?>"></script>
            <script>
                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', '<?=$ga_id?>');
            </script>
        <?php
        endif;
    endif;
} );


function site_delete_transients() {
    delete_transient( 'latest_posts' );
    delete_transient( 'posts_wordpress_tag' );
    delete_transient( 'sticky_posts' );
    delete_transient( 'latest_books' );
}

add_action( 'delete_post', __NAMESPACE__ . '\\site_delete_transients' );
add_action( 'edit_post', __NAMESPACE__ . '\\site_delete_transients' );
add_action( 'post_updated', __NAMESPACE__ . '\\site_delete_transients' );
add_action( 'save_post', __NAMESPACE__ . '\\site_delete_transients' );

add_action( 'breadcrumbs', function () {
    if ( function_exists( 'yoast_breadcrumb' ) ) :
        yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
    endif;
} );


/**
 * Inject the table of content on each post.
 */

function get_entry_content_titles( $content ) {

    global $table_of_contents;

    $table_of_contents = [];

    return preg_replace_callback( '#<(h[1-6])(.*?)>(.*?)</\1>#si', function ( $matches ) use ( &$table_of_contents ) {
        $tag    = $matches[ 1 ];
        $title  = strip_tags( $matches[ 3 ] );
        $has_id = preg_match( '/id=(["\'])(.*?)\1[\s>]/si', $matches[ 2 ], $matched_ids );
        $id     = $has_id ? $matched_ids[ 2 ] : sanitize_title( $title );

        array_push( $table_of_contents, sprintf( '<li><a href="#%s">%s</a></li>', $id, $title ) );

        if ( $has_id ) {
            return $matches[ 0 ];
        }

        return sprintf( '<%s%s id="%s">%s</%s>', $tag, $matches[ 2 ], $id, $matches[ 3 ], $tag );

    }, $content );
}

add_filter( 'the_content', __NAMESPACE__ . '\\get_entry_content_titles' );


/**
 * Get Table of Contents
 */
function get_the_table_of_contents() {
    if ( !is_single() ) {
        return;
    }
    global $table_of_contents;

    if ( count( $table_of_contents ) > 0 ) : ?>
        <section class="widget-table-of-contents">
            <h3>Indice</h3>
            <ul class="list-unstyled">
                <?php foreach ( $table_of_contents as $element ) :
                    echo $element;
                endforeach; ?>
            </ul>
        </section>
    <?php endif;
}

add_action( 'sidebar_action', __NAMESPACE__ . '\\get_the_table_of_contents' );
